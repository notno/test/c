FROM python:alpine

RUN apk add --no-cache flawfinder cppcheck

RUN pip install cppcheck-junit

ENTRYPOINT [""]
